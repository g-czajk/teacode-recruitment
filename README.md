## Instructions on running locally

**Clone this repo:**

```
  git clone https://gitlab.com/g-czajk/teacode-recruitment.git
```
**Go to the project directory:**

```
   cd teacode-recruitment
```
**Install dependencies:**

```
   npm install
```
**Start dev server:**

```
   npm start
```
